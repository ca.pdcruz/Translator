<?php
include("editor.php");

$project = new Project(@$_GET["project"]);
$language = new Language($project, @$_GET["language"]);

echo "<h1>".$_GET["project"]."</h1> &nbsp; ";
echo "<span class=\"notice-breakline\">Translating into ".$language->getHumanReadableName()."</span>";

if (@$_GET["finished"] == "true" && !$language->isFinished()) {
    $language->setFinished(true);
    Email::sendFinishedMail($project, $language);
}

if ($language->isFinished()) {
    echo "<p class=\"success\">✔ Translation marked as finished. Editing no longer possible.</p>";
} else {
    echo "<br /><a class=\"button\" style=\"width:200px;\" href=\"".getLink()."&finished=true\">✔ Mark as finished</a>";
    echo "&nbsp; <span class=\"notice-breakline\">It is no longer possible to edit the translations after clicking this button.</span><br />";
}

$table = new TranslationTable($project, $language);
$table->printHead();
$table->printTranslator();

foreach ($project->getResources() as $resource) {
    if ($resource->getName() == "translator") {
        continue;
    }
    switch ($resource->getType()) {
        case Resource::TYPE_TEXT:
            $table->translateFile($resource);
            break;
        case Resource::TYPE_ARRAY:
            $table->translateArrays($resource);
            break;
        case Resource::TYPE_STRING:
            $table->translateStrings($resource);
            break;
    }
}

$table->printFooter();
