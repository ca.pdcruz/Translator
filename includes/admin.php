<?php

echo "<h1>Translator</h1> &nbsp; <span class=\"notice-breakline\">Admin console</span>"
. "<a class=\"button secondary\" href=\"./?admin=false\" style=\"float:right;margin-top: 20px;\">Logout</a><br />";

if(isset($_POST["new-password"])) {
    $jsonText = @file_get_contents("./files/config.json");
    $config = json_decode($jsonText, true);
    $config["admin-password"] = $_POST["new-password"];
    file_put_contents("./files/config.json", json_encode($config, JSON_PRETTY_PRINT));
    $_SESSION["password"] = $_POST["new-password"];
    echo "<p class=\"success\">✔ Password changed successfully.</p>";
}
if(isset($_POST["add-project-name"])) {
    $project = Project::create($_POST["add-project-name"]);
    if ($project !== null) {
        echo "<p class=\"success\">✔ Project created successfully.</p>";
    } else {
        echo "<p class=\"error\">✘ Project already exists.</p>";
    }
}

$jsonText = @file_get_contents("./files/config.json");
$config = json_decode($jsonText, true);
$password = @$config["admin-password"];
if ($password == "") {
    echo "<p class=\"error\">✘ Please set an admin password</p>";
}

if(@$_GET["do"] == "password") {
    echo "<h2>Set admin password</h2>";
    echo "<form class=\"simple\" method=\"POST\" action=\"./\">";
    echo "<input spellcheck=\"false\" class=\"box\" id=\"new-password\" name=\"new-password\" value=\"\" />";
    echo "<input type=\"submit\" class=\"submit\" value=\"Set\">";
    echo "<a class=\"button secondary\" href=\"./\">Cancel</a><br />";
    echo "<script>setTimeout(function() { document.getElementById('new-password').focus(); }, 10);</script>";
    echo "</form>";
} else if(@$_GET["do"] == "add_project") {
    echo "<h2>Add project</h2>";
    echo "<form class=\"simple\" method=\"POST\" action=\"./?\">";
    echo "<input spellcheck=\"false\" class=\"box\" id=\"add-project-name\" name=\"add-project-name\" />";
    echo "<input type=\"submit\" class=\"submit\" value=\"Add\">";
    echo "<a class=\"button secondary\" href=\"./\">Cancel</a><br />";
    echo "<script>setTimeout(function() { document.getElementById('add-project-name').focus(); }, 10);</script>";
    echo "</form>";
}

$project = new Project(@$_GET["project"]);
if (!$project->exists()) {
    echo "<h2>Projects</h2><br />";

    if (count(Project::getProjects()) == 0) {
        echo "<p class=\"warning\">Create a project to get started</p>";
    }
    foreach (Project::getProjects() as $project) {
        echo "<a class=\"button inline\" href=\"./?project=".$project->getName()."\">".$project->getName()."</a>";
    }

    echo "<br /><br /><h2>Actions</h2><br />";
    echo "<a class=\"button inline\" href=\"./?do=password\">Change admin password</a>";
    echo "<a class=\"button inline\" href=\"./?do=add_project\">Add project</a>";
} else if(isset($_GET["verify"])) {
    include("verifyLanguage.php");
} else if(@$_GET["do"] == "components") {
    include("manageComponents.php");
} else if ($project->exists()) {
    include("projectDetails.php");
}
