<?php

$project = new Project(@$_GET["project"]);
$language = new Language($project, @$_GET["verify"]);

if (!$language->exists()) {
    echo "<p class='error'>Language does not exist</p>";
} else {

    $allStrings = "";
    foreach ($project->getResources() as $resource) {
        $allStrings .= strip_tags($resource->getRaw($language))."\n";
    }
    
    echo "<a class=\"button\" style=\"width: 200px;\" href=\"https://translate.google.de/#auto/en/".urlencode($allStrings)."\">Google Translate</a>";
    echo "<br /><br /><div class=\"infobox\">";
    echo preg_replace("/\n+/", "<br />", $allStrings);
    echo "</div>";
}
