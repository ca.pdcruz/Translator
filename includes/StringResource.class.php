<?php

class StringResource extends Resource {

    public function getType() {
        return Resource::TYPE_STRING;
    }
    
    public function save(Language $language, $id, $value) {
        $xmlContent = $this->getRaw($language);
        $xml = new SimpleXMLElement(removeBom($xmlContent));
        $saved = false;
        foreach ($xml->string as $string) {
            if ($id === "string--".$string["name"]) {
                $string[0] = toXML($value);
                $saved = true;
            }
        }
        if (!$saved) {
            $new = $xml->addChild("string", toXML($value));
            $new->addAttribute("name", str_replace("string--", "", $id));
            $saved = true;
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml->asXML());
        $this->putRaw($language, $dom->saveXML());
        return $saved;
    }

    public function bootstrap(Language $language) {
        $this->putRaw($language, "<?xml version=\"1.0\" encoding=\"utf-8\"?><resources></resources>");
        return true;
    }
}
