<?php

class TextResource extends Resource {

    public function getType() {
        return Resource::TYPE_TEXT;
    }
    
    public function save(Language $language, $id, $value) {
        $this->putRaw($language, $value);
        return true;
    }

    public function bootstrap(Language $language) {
        $this->putRaw($language, "");
        return true;
    }
}
