<?php

$previousWasTranslated = false;

function editor($id, $translation, $original, $source) {
    global $project,$language,$previousWasTranslated;

    $path = $project->getPath() . "/values";
    $saved = false;
    $ret = "";
    if(isset($_POST["id"]) && @$_POST["id"] == $id) {
        $resource = $project->findResource($source);
        if ($source === "translator") {
            file_put_contents($language->getPath() . "/translator", $_POST["translated"]);
            $translation = $_POST["translated"];
        } else if ($resource == null) {
            die("Resource not found.");
        } else {
            $saved = $resource->save($language, $id, $_POST["translated"]);
            if ($resource->getType() == Resource::TYPE_TEXT) {
                $translation = $_POST["translated"];
            } else {
                $translation = toXML($_POST["translated"]);
            }
        }
    }

    if (@$_GET["id"] == $id && !$language->isFinished()) {
        $ret .= "<form name=\"editor\" action=\"".getLink()."#$id\" method=\"POST\">";
        $ret .= "<input type=\"hidden\" name=\"id\" value=\"$id\" />";
        if ($source == "store.txt") {
            $ret .= "<textarea id=\"editor\" style=\"height: 300px;\" name=\"translated\">".toTextarea($translation)."</textarea>";
        } else {
            $ret .= "<textarea id=\"editor\" name=\"translated\">".toTextarea($translation)."</textarea>";
        }

        $ret .= "<input class=\"submit\" type=\"submit\" value=\"Save\">";
        $ret .= "<a class=\"button secondary\" href=\"".getLink()."\">Cancel</a>";
        $ret .= "<span class=\"notice-breakline\">🛈 Press Ctrl+Enter to save.</span>";
        $ret .= "</form><script>setTimeout(function() { document.getElementById('editor').focus(); }, 10);</script>";
        if(strpos($original,"\n") === false && strpos($original,"\\n") === false) {
            $ret .= "<script>document.getElementById('editor').onkeypress = keypressOneLine;</script>";
        } else {
            $ret .= "<script>document.getElementById('editor').onkeypress = keypressMultiLine;</script>";
        }
        return $ret;
    } else {
        if ($translation != "" && $translation != pack("CCC",0xef,0xbb,0xbf)) {
            $ret .= toHTML($translation);
            $ret .= "<br /><br />";
        }
        if (!$language->isFinished()) {
            if ($source == "translator") {
                $ret .= "<a class=\"button\" href=\"".getLink()."&id=$id#$id\">Edit</a>";
            } else {
                if($previousWasTranslated) {
                    $ret .= "<a class=\"button\" id=\"next-to-translate\" href=\"".getLink()."&id=$id#$id\">Translate</a>";
                } else {
                    $ret .= "<a class=\"button\" href=\"".getLink()."&id=$id#$id\">Translate</a>";
                }
            }
        }
        if ($previousWasTranslated) {
                $ret .= "<script>setTimeout(function() { document.getElementById('next-to-translate').focus(); }, 50);</script>";
                $ret .= "<span class=\"notice-breakline\">🛈 Press Enter to translate.</span>";
                $previousWasTranslated = false;
        }
        if ($saved) {
            $ret .= "<span class=\"notice-breakline hide-animation\">✔ Saved</span>";
            $ret .= "<script>setTimeout(function() { document.getElementById('editor').focus(); }, 50);</script>";
            $previousWasTranslated = true;
        }
    }
    $ret .= "<a class=\"anchor\" name=\"$id\">&nbsp;</a>";
    return $ret;
}
