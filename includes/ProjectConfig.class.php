<?php

class ProjectConfig {
    private $config;
    private $project;

    public function __construct(Project $project) {
        $this->project = $project;
        $jsonText = @file_get_contents($project->getPath() . "/config.json");
        $this->config = json_decode($jsonText, true);
    }

    public function getResources() {
        $resources = array();
        foreach ($this->config["resources"] as $jsonResource) {
            switch ($jsonResource["type"]) {
                case "text":
                    $resource = new TextResource($this->project, $jsonResource["filename"]);
                    break;
                case "array":
                    $resource = new ArrayResource($this->project, $jsonResource["filename"]);
                    break;
                case "string":
                    $resource = new StringResource($this->project, $jsonResource["filename"]);
                    break;
                default:
                    die("Invalid resource type in config file");
                    break;
            }

            $resource->setDescription($jsonResource["description"]);

            if (isset($jsonResource["destinationPath"])) {
                $resource->setDestinationPath($jsonResource["destinationPath"]);
            } else {
                $resource->setDestinationPath(NULL);
            }

            $resources[] = $resource;
        }
        return $resources;
    }

    public function updateResource(Resource $resource, string $key, string $value) {
        foreach ($this->config["resources"] as $resourceNumber => $jsonResource) {
            if ($jsonResource["filename"] === $resource->getName()) {
                $this->config["resources"][$resourceNumber][$key] = $value;
                file_put_contents($this->project->getPath() . "/config.json",
                        json_encode($this->config, JSON_PRETTY_PRINT));
                break;
            }
        }
    }

    public function deleteResource(Resource $resource) {
        foreach ($this->config["resources"] as $resourceNumber => $jsonResource) {
            if ($jsonResource["filename"] === $resource->getName()) {
                unset($this->config["resources"][$resourceNumber]);
                @unlink("./files/" . $_GET["project"] . "/values/" . $resource->getName());
                foreach ($this->project->getLanguages() as $language) {
                    @unlink($language->getPath() . "/" . $resource->getName());
                }
                file_put_contents($this->project->getPath() . "/config.json",
                        json_encode($this->config, JSON_PRETTY_PRINT));
                break;
            }
        }
    }

    public function createResource(string $filename, string $type) {
        $jsonResource = array("filename" => $filename,
            "destinationPath" => "\$languageCode/$filename",
            "destinationPath" => "\$languageCode/$filename",
            "type" => $type,
            "description" => "New component: $filename");
        $this->config["resources"][] = $jsonResource;
        file_put_contents($this->project->getPath() . "/config.json",
                json_encode($this->config, JSON_PRETTY_PRINT));
    }
}
