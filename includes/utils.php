<?php

spl_autoload_register(function ($class_name) {
    include "./includes/$class_name.class.php";
});

function startsWith($haystack, $needle) {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }
    return (substr($haystack, -$length) === $needle);
}

function file_put_contents_utf8($filename,$content) {
    $f=fopen($filename,"w");
    # Now UTF-8 - Add byte order mark
    if(!startsWith($content,pack("CCC",0xef,0xbb,0xbf))) {
        fwrite($f, pack("CCC",0xef,0xbb,0xbf));
    }
    fwrite($f,$content);
    fclose($f);
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getLink() {
    $ret = "./?project=".$_GET["project"]."&access=".
           $_GET["access"]."&language=".$_GET["language"];
    return $ret;
}
function toHTML($str) {
    $str = str_replace("\n", "<br />", toTextarea($str));
    return $str;
}
function toTextarea($str) {
    $str = str_replace(">", "&gt;", $str);
    $str = str_replace("<", "&lt;", $str);
    $str = str_replace("<", "&lt;", $str);
    $str = str_replace("\\'", "'", $str);
    $str = str_replace("\\\"", "\"", $str);
    $str = str_replace("\\\\", "--doublebackslash--", $str);
    $str = str_replace("\\n", "\n", $str);
    $str = str_replace("--doublebackslash--", "\\", $str);
    return $str;
}
function toPlainHTML($str) {
    $str = str_replace(">", "&gt;", $str);
    $str = str_replace("<", "&lt;", $str);
    $str = str_replace(" ", "&nbsp;", $str);
    return $str;
}
function fromPlainHTML($str) {
    $str = str_replace("\xC2\xA0", " ", $str);
    //$str = str_replace("\xA0", " ", $str);
    $str = str_replace("\r", "", $str);
    if (!endsWith($str, "\n")) {
        $str .= "\n";
    }
    $str = str_replace("\n", "\n", $str);
    return $str;
}
function toXML($str) {
    $str = str_replace("\r", "", $str);
    $str = str_replace("\\", "\\\\", $str);
    $str = str_replace("'", "\\'", $str);
    $str = str_replace("\"", "\\\"", $str);
    $str = str_replace("\n", "\\n", $str);
    $str = str_replace("&gt;",">", $str);
    $str = str_replace("&lt;","<", $str);
    return $str;
}
function xcopy($src, $dest) {
    foreach (scandir($src) as $file) {
        $srcfile = rtrim($src, '/') .'/'. $file;
        $destfile = rtrim($dest, '/') .'/'. $file;
        if (!is_readable($srcfile)) {
            continue;
        }
        if ($file != '.' && $file != '..') {
            if (is_dir($srcfile)) {
                if (!file_exists($destfile)) {
                    mkdir($destfile);
                }
                xcopy($srcfile, $destfile);
            } else {
                copy($srcfile, $destfile);
            }
        }
    }
}
function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function removeBom($str) {
    return str_replace("\xEF\xBB\xBF",'',$str);
}

function contains($haystack, $needle) {
    return strpos($haystack, $needle) !== false;
}

function stringEmpty($string) {
    return trim(removeBom($string)) === "";
}
