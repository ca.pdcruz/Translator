<?php

class Session {
    public static function isLoggedIn() {
        $jsonText = @file_get_contents("./files/config.json");
        $config = json_decode($jsonText, true);
        $password = @$config["admin-password"];
        if ($password === @$_SESSION["password"] || $password === @$_GET["password"]) {
            return true;
        } else {
            return false;
        }
    }

    public static function logout() {
        $_SESSION["password"] = "";
    }

    public static function login($password) {
        $_SESSION["password"] = $password;
    }
}
