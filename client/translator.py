#!/bin/python3
from pathlib import Path
from urllib.request import urlopen, URLError, HTTPError
import sys
import urllib.request
import os
import zipfile

################ Read config ################

configfile = Path.home() / '.translaterc'

if not configfile.is_file():
    print("Config file not found in " + str(configfile.resolve()))
    exit()

config = {}
with open(configfile) as myfile:
    for line in myfile:
        name, var = line.partition("=")[::2]
        config[name.strip()] = str(var).strip()
        
if config["host"][len(config["host"])-1] != "/":
    config["host"] = config["host"] + "/"

print("Using host " + config["host"])

################ Get project ################


projectRoot = Path('.')
while not ((projectRoot / '.translator').is_file() or str(projectRoot.resolve()) == "/"):
  projectRoot = projectRoot / '..'

configfile = (projectRoot / '.translator')
if not configfile.is_file():
    print("Not using a project direcory. Create a .translator file!")
    exit()

################## Do stuff #################

if len(sys.argv) == 1:
    print("No arguments. Try 'pull'")
elif sys.argv[1] == "pull":    
    with open(configfile) as myfile:
        for line in myfile:
            name, var = line.partition("=")[::2]
            config[name.strip()] = str(var).strip()
        
    url = config["host"] + "downloader.php?project=" + config["project"] + "&password=" + config["password"]
    localTemp = 'translator.zip~'
    
    try:
        f = urlopen(url)
        with open(localTemp, "wb") as local_file:
            local_file.write(f.read())

        with zipfile.ZipFile(localTemp, "r") as zip_ref:
            zip_ref.extractall(str(projectRoot.resolve()))

        print("Done")
    except HTTPError as e:
        print("Error: " + e.read().decode("utf-8"))
    except zipfile.BadZipFile:
        print("Error: Bad zip file")
    finally:
        if Path(localTemp).is_file():
            os.remove(localTemp)
